package com.gmail.lukasmcd14.irc.bot;

import java.net.Socket;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class Bot {

    public ArrayList<Channel> channels;
    String _channel, _password, _username, _address;
    int _port;

    public synchronized void connect(String address, String username, String password){
        connect(address, 8887, username, password);
    }

    public synchronized void connect(String address, int port, String username, String password){

        _address = address;
        _port = port;
        _username = username;
        _password = password;


        Socket connection = null;
    }

    public int getMaxLineLength() {
        return ServerInput.MAX_LINE_LENGTH;
    }
}
