package com.gmail.lukasmcd14.irc.bot;

import java.io.BufferedWriter;

@SuppressWarnings("ALL")
public class ServerOutput extends Thread {


    ServerOutput(){

    }

    static void sendRawLine(Bot bot, BufferedWriter bwriter, String line) {
        if (line.length() > bot.getMaxLineLength() - 2) {
            line = line.substring(0, bot.getMaxLineLength() - 2);
        }
        synchronized(bwriter) {
            try {
                bwriter.write(line + "\r\n");
                bwriter.flush();
            }
            catch (Exception e) {
            }
        }
    }

}
