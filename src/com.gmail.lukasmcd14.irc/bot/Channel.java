package com.gmail.lukasmcd14.irc.bot;

@SuppressWarnings("unused")
public class Channel {

    String channel_name;
    User[] users;

    Channel(String name){
        channel_name = name;
    }

    public User[] getUsers(){
        return users;
    }

}
