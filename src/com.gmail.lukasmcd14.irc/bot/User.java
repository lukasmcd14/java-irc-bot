package com.gmail.lukasmcd14.irc.bot;

public class User {

    String username = "";
    boolean isOp = false;

    User(String name, boolean op){
        this.username = name;
        this.isOp = op;
    }

    public boolean isOp(){
        return isOp;
    }

    public String getUsername(){
        return username;
    }

    public String toString(){
        return "User:[Username: " + getUsername() + ", Mod: " + isOp() + "]";
    }

}
