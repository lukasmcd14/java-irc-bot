package com.gmail.lukasmcd14.irc.bot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.Socket;

@SuppressWarnings("unused")
public class ServerInput {

    Socket irc = null;
    Bot ircbot = null;
    private BufferedReader br = null;
    private BufferedWriter bw = null;
    private boolean connected = true;
    private boolean disposed = false;
    public static final int MAX_LINE_LENGTH = 512;

    ServerInput(Bot bot, Socket server, BufferedReader reader, BufferedWriter writer){
        ircbot = bot;
        irc = server;
    }

}
